package com.tn.medianet.backend.controller;


import com.tn.medianet.backend.model.Candidat;
import com.tn.medianet.backend.services.CandidatService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;
//http://www.programmersought.com/article/70322210279/
@RestController
@RequestMapping(value ="/admin")
@CrossOrigin(origins = "*")
public class CandidatController {



    @Autowired
    private CandidatService candidatService;


    //************* All Test***********
    @RequestMapping(value = "/candidat/getAllCandidat", method = RequestMethod.GET)
    public ResponseEntity getAllCandidat(){

        List<Candidat> listcandiat=this.candidatService.getAllCandiat();


        if(listcandiat!=null){
            if(listcandiat.size()>0){
                return new ResponseEntity<>(listcandiat, HttpStatus.OK);

            }
            else{
                return new ResponseEntity<>( HttpStatus.NOT_FOUND);

            }
        }
        return new ResponseEntity<>( HttpStatus.NOT_FOUND);

    }

    //** Save Or Update
    @PutMapping(value="/candidat/creerUpdateCandidat/{id}")
    public ResponseEntity SaveOrUpdateCandidat(@RequestBody Candidat candidat,@PathVariable("id") int id) {
        candidat.setIdCandidat(id);
        afficheText("candidat info "+candidat.toString());
        Candidat candidat1=this.candidatService.save(candidat);

        if(candidat!=null){
            return new ResponseEntity<>(candidat1,HttpStatus.OK);

        }
        return new ResponseEntity<>(HttpStatus.NOT_FOUND);

    }
    //** Save Or Update
    @PostMapping(value="/candidat/creerCandidat/")
    public ResponseEntity SaveCandidat(@RequestBody Candidat candidat) {
        afficheText("candidat info "+candidat.toString());
        Candidat candidat1=this.candidatService.save(candidat);

        if(candidat!=null){
            return new ResponseEntity<>(candidat1,HttpStatus.OK);

        }
        return new ResponseEntity<>(HttpStatus.NOT_FOUND);

    }



    //**********Get QuestionQuiz By Id
    @GetMapping(value="/candidat/getCandidatById/{id}")
    public ResponseEntity getCandidatById(@PathVariable("id") int id_candidat) {
        Candidat candidat=null;


        candidat=this.candidatService.getCandiatById(id_candidat);

        if(candidat!=null){
            afficheText("Candidat"+candidat.toString());
            return new ResponseEntity<>(candidat, HttpStatus.OK);

        }
        return new ResponseEntity<>( HttpStatus.NOT_FOUND);

    }

    @DeleteMapping("/candidat/delete/{id}")
    public ResponseEntity deleteStudent(@PathVariable("id") int id) {

        Candidat candidat=null;
        Boolean b=false;
        candidat=this.candidatService.getCandiatById(id);
        afficheText("Candidat");
        afficheText("Delete : get candiat by id  "+candidat.toString());

        if(candidat!=null)
        {
            this.candidatService.delete(candidat);
            b=true;
            return new ResponseEntity<>( b,HttpStatus.OK);

        }
        afficheText("Delete:Not Existing student by this id "+id);

        return new ResponseEntity<>(false, HttpStatus.NOT_FOUND);


    }




    public void afficheText(String msg)
    {
        System.out.println(msg);
    }





    public CandidatService getCandidatService() {
        return candidatService;
    }

    public void setCandidatService(CandidatService candidatService) {
        this.candidatService = candidatService;
    }
}
