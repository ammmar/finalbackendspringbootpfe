package com.tn.medianet.backend.controller;


import com.tn.medianet.backend.model.Choix;
import com.tn.medianet.backend.model.Question;
import com.tn.medianet.backend.services.ChoixService;
import com.tn.medianet.backend.services.QuestionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.constraints.Positive;
import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping(value ="/admin")
@CrossOrigin(origins = "*")
public class ChoixController {

    @Autowired
    private ChoixService choixService;

    @Autowired
    private QuestionService questionService;

    //************ Creer Test
    @PutMapping(value="/choix/creerChoix/{id}")
    public ResponseEntity AjouterChoix(@RequestBody Choix choix, @PathVariable("id") int id_question) {

        Question questionSelected=this.questionService.getQuestionById(id_question);
        if(questionSelected!=null){
            choix.setQuestion(questionSelected);
            this.afficheText("choix posted is ==>"+questionSelected.toString());

            Choix savedChoix=this.choixService.saveChoix(choix);

            if(savedChoix!=null)
            {
                return new ResponseEntity<>( savedChoix,HttpStatus.OK);

            }
            else {
                return new ResponseEntity<>( HttpStatus.NOT_FOUND);

            }

        }
        return new ResponseEntity<>( HttpStatus.NOT_FOUND);


    }



    @PutMapping(value="/choix/EnregistrerStateQuestionChoix/{id}")
    public ResponseEntity  EnregistrerStateQuestionChoix(@RequestBody Choix choix, @PathVariable("id") int id_question) {

        this.afficheText("choix     "+choix.getChoix()+"  reponse     "+choix.getReponse()+"   id choix    "+choix.getIdChoix());
        List<Choix>listChoix=new ArrayList<Choix>();


        Question questionSelected=this.questionService.getQuestionById(id_question);

        listChoix=questionSelected.getChoixs();
        Choix c=new Choix();
        int taille=listChoix.size();
        if(questionSelected!=null){
            choix.setQuestion(questionSelected);
            this.afficheText("choix posted is ==>"+questionSelected.toString());
            if (taille > 0) {
                this.afficheText("size list choix of question " + questionSelected.getDesgnQuestion() + "Size======>" + taille);
            }
            Choix savedChoix=new Choix();
            //update choix operation for exist id choix
            if(choix.getIdChoix()>0) {
                savedChoix = this.choixService.saveChoix(choix);
                this.afficheText("  update choix operation for exist id choix\n"+savedChoix.toString());
                for (int i = 0; i < listChoix.size(); i++) {
                    c = listChoix.get(i);
                    if (c.getChoix().equals(savedChoix.getChoix()) == false) {
                        c.setReponse(0);
                        this.choixService.saveChoix(c);
                    }
                }
            }
            //update choix operation without id choix
            else if(choix.getIdChoix()==0){

                for (int i = 0; i < listChoix.size(); i++) {
                    c = listChoix.get(i);
                    if (c.getChoix().equals(choix.getChoix()) == true) {
                        c.setReponse(1);
                    savedChoix  =this.choixService.saveChoix(c);
                    this.afficheText("update choix operation without id choix"+savedChoix.toString());
                    }
                    else{
                        c.setReponse(0);
                        this.choixService.saveChoix(c);
                    }
                }
            }
          // this.afficheText("*******EnregistrerStateQuestionChoix**** number of row modifiy  is=>"+ this.choixService.updateChoix(choix.getChoix(),id_question));
            if(savedChoix!=null)
            {
                return new ResponseEntity<>( savedChoix,HttpStatus.OK);

            }
            else {
                return new ResponseEntity<>( HttpStatus.NOT_FOUND);

            }

        }
        return new ResponseEntity<>( HttpStatus.NOT_FOUND);
    }






    public void afficheText(String msg)
    {
        System.out.println("*********ChoixController********"+msg);
    }


}
