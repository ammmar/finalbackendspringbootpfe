package com.tn.medianet.backend.controller;


import com.tn.medianet.backend.model.MailBody;
import com.tn.medianet.backend.model.UserModel;
import com.tn.medianet.backend.services.SmtpMailSender;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.mail.MailException;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.JavaMailSenderImpl;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.web.bind.annotation.*;

import javax.mail.internet.MimeMessage;
import java.util.Properties;

@RestController
@RequestMapping(value ="/admin")
@CrossOrigin(origins = "*")
public class MailController {
    @Autowired
    private SmtpMailSender smtpMailSender;

    @Autowired
    private JavaMailSender sender;

    @PostMapping("/contact/mail")
    public ResponseEntity  signupSuccess( @RequestBody MailBody mailBody) {

        UserModel usermodel = new UserModel();
        usermodel.setSubject(mailBody.getSubject());
        usermodel.setMessage(mailBody.getMessage());
        usermodel.setEmail(mailBody.getEmail());
        usermodel.setName(mailBody.getName());

        try {
            //smtpMailSender.sendNotification(usermodel);
            JavaMailSenderImpl mailSender = new JavaMailSenderImpl();
            mailSender.setHost("smtp.gmail.com");
            mailSender.setPort(587);

            mailSender.setUsername("ammar.hamza1995@gmail.com");
            mailSender.setPassword("hamzaammar123");

            Properties props = mailSender.getJavaMailProperties();
            props.put("mail.transport.protocol", "smtp");
            props.put("mail.smtp.auth", "true");
            props.put("mail.smtp.starttls.enable", "true");
            props.put("mail.debug", "true");
            props.put("mail.smtp.ssl.trust", "smtp.gmail.com");

            this.sender=mailSender;

            StringBuilder sb = new StringBuilder();
            sb.append("Name: " + usermodel.getName()).append(System.lineSeparator());
            sb.append("\n Message: " + usermodel.getMessage());

            SimpleMailMessage mail = new SimpleMailMessage();

            mail.setTo(usermodel.getEmail());
            mail.setFrom("ammar.hamza1995@gmail.com");

            mail.setSubject(usermodel.getSubject());

            mail.setText(sb.toString());

            this.sender.send(mail);
            return new ResponseEntity<>(mail, HttpStatus.OK);

        }catch(MailException e) {

            System.out.println("Mail excepetion"+e.getMessage());
            return new ResponseEntity<>( e.getMessage(),HttpStatus.NOT_FOUND);

            //

        }


        //  return "redirect:/api/contact";
    }




}
