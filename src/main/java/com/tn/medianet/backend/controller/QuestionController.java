package com.tn.medianet.backend.controller;

import com.tn.medianet.backend.model.Choix;
import com.tn.medianet.backend.model.Question;
import com.tn.medianet.backend.model.Test;
import com.tn.medianet.backend.model.Testquestion;
import com.tn.medianet.backend.services.ChoixService;
import com.tn.medianet.backend.services.QuestionService;
import com.tn.medianet.backend.services.TestQuestionService;
import com.tn.medianet.backend.services.TestService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.nio.file.Path;
import java.util.*;

@RestController
@RequestMapping(value ="/admin")
@CrossOrigin(origins = "*")
public class QuestionController {

    @Autowired
    private QuestionService questionService;

    @Autowired
    private TestService testService;

    @Autowired
    private ChoixService choixService;

    @Autowired
    private TestQuestionService testQuestionService;

    @RequestMapping(value = "/question/getallQuestionUnique", method = RequestMethod.GET)
    public ResponseEntity getallQuestionUnique() {
        List<Question>listq=new ArrayList<Question>();
        List<Question>listqUnique=new ArrayList<Question>();
        List<String>ListQuestionString=new ArrayList<String>();


        listq=this.questionService.getAllQuestionDistinct();
        Question question=new Question();
        int j=0;
        for(int i=0;i<listq.size();i++){
            question=new Question();
            listq.get(i).setDisabled(false);
            listq.get(i).setChecked(false);
        }
        if(listq.size()>0)
        {
            return new ResponseEntity<>(listq,HttpStatus.OK);
        }
        return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }



    @RequestMapping(value = "/question/getallQuestion", method = RequestMethod.GET)
    public ResponseEntity getallQuestion(){
        List<Question>listq=new ArrayList<Question>();

        listq=this.questionService.getAllQuestion();
        int j=0;
        for(int i=0;i<listq.size();i++){
            listq.get(i).setDisabled(false);
            listq.get(i).setChecked(false);



        }
        if(listq.size()>0)
        {
            return new ResponseEntity<>(listq,HttpStatus.OK);

        }
        return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }

    @DeleteMapping(value = "/question/deletequestion/{id}")
    public ResponseEntity DeleteQuestion(@PathVariable("id")int id_question){
       Question question=this.questionService.getQuestionById(id_question);
        if(question!=null)
        {
            this.questionService.delete(question);
            return new ResponseEntity<>(question,HttpStatus.OK);

        }
        else{
            this.afficheText("question not found"+id_question);
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }

    }



    //** Save Or Update
    @PostMapping(value="/question/creerUpdateQuestion/")
    public ResponseEntity SaveOrUpdateQuestion(@RequestBody Question question) {
        afficheText("QuestionQuiz:id question"+question.getIdQuestion());
        question.setEtatOrigin(1);
    Question q=this.questionService.save(question);
    if(q!=null){
        return new ResponseEntity<>(q,HttpStatus.OK);

    }
        return new ResponseEntity<>(HttpStatus.NOT_FOUND);

    }



    public   String saveTestQuestion(Test t,Question q){
        Testquestion testquestion=new Testquestion();
        if(t!=null) {
            testquestion.setQuestion(q);
            testquestion.setTest(t);
            afficheText("TestQuestion"+testquestion.toString());
            Testquestion TQsaved=this.testQuestionService.ajouter(testquestion);
            this.afficheText("QuestionController:CreerTest:TestQuestionSaved"+TQsaved.toString());
           return "QuestionController:CreerTest:TestQuestionSaved"+TQsaved.toString();
        }else{
            this.afficheText("probleme d'ajouter TestQuestion");
            return "probleme d'ajouter TestQuestion";
        }



    }


    @PutMapping(value="/question/creerQuestionListToTest/{id}")
    public ResponseEntity AffecterQuestionListToTest(@PathVariable("id") int idTest
            ,@RequestBody  List<Question>listQuestion)
    {
        Optional<Test> testOptional=this.testService.getTestById(idTest);
        Test test=testOptional.get();
        Test t=new Test();
        Testquestion testquestion=new Testquestion();
        Optional<Test> to=this.testService.getTestById(idTest);
        if(to.isPresent()){
            t=to.get();
        }
        Boolean b=false;
        List<Testquestion>testquestionList=new ArrayList<Testquestion>();
        for(int i=0;i<listQuestion.size();i++){
            Question question=new Question();
            question=listQuestion.get(i);
            question.setEtatOrigin(0);
            Question q=this.questionService.saveOneQuestion(question);
          testquestion=new Testquestion();
          testquestion.setTest(t);
          testquestion.setQuestion(q);

          //  testquestionList.add(testquestion);
            this.saveTestQuestion(t,q);
            b=true;
        }
      //  t.setTestquestions(testquestionList);
        //this.testService.Save(t);

        if(b==true){
            return new ResponseEntity<>(true,HttpStatus.OK);

        }
        return new ResponseEntity<>(false,HttpStatus.NOT_FOUND);

    }


    //************ Creer Test
    @PutMapping(value="/question/creerQuestion/{id}")
    public ResponseEntity AjouterQuestionTest(@RequestBody Question question, @PathVariable("id") int id_test) {

        Random objGenerator = new Random();
        int randomNumber =30+ objGenerator.nextInt(10000);
        System.out.println("Random No : " + randomNumber);
        question.setIdQuestion(randomNumber);
        Question questionSaved=new Question();
        question.setEtatOrigin(1);
       questionSaved=this.questionService.save(question);

        Test t=new Test();
        Testquestion testquestion=new Testquestion();

        Optional<Test> to=this.testService.getTestById(id_test);
        if(to.isPresent()){
            t=to.get();
        }
        afficheText("saved question  "+questionSaved.toString());

        if(t!=null) {
            testquestion.setQuestion(questionSaved);
            testquestion.setTest(t);
            afficheText("TestQuestion"+testquestion.toString());
            Testquestion TQsaved=this.testQuestionService.ajouter(testquestion);
            afficheText("QuestionController:CreerTest:TestQuestionSaved"+TQsaved.toString());
        }else{
        afficheText("probleme d'ajouter TestQuestion");
        }


            if(questionSaved!=null) {

                return new ResponseEntity<>(question,HttpStatus.OK);
            }
            else{
                return new  ResponseEntity(HttpStatus.NOT_FOUND);
            }

    }





    //**********Get QuestionQuiz By Id
    @GetMapping(value="/question/getQuestiontById/{id}")
    public ResponseEntity getTestById(@PathVariable("id") int id) {
        Question q=this.questionService.getQuestionById(id);
        if(q!=null)
        return new ResponseEntity<>(q, HttpStatus.OK);
        else
            return new ResponseEntity<>( HttpStatus.NOT_FOUND);
    }



    //**************** partie choix

    //****** get choix by question id

    @GetMapping(value="/question/getChoixsByQuestionId/{id}")
    public ResponseEntity getchoixsByQuestionId(@PathVariable("id") int id_question){
        List<Choix>listchoix=new ArrayList<Choix>();
        Question q=this.questionService.getQuestionById(id_question);
        if(q!=null){
            listchoix=q.getChoixs();
            if(listchoix.size()>0){
                return new ResponseEntity<>( listchoix,HttpStatus.OK);

            }else {
                return new ResponseEntity<>( HttpStatus.NOT_FOUND);
            }
        }else{
            return new ResponseEntity<>( HttpStatus.NOT_FOUND);

        }
    }


    public void afficheText(String msg)
    {
        System.out.println(msg);
    }



    public TestService getTestService() {
        return testService;
    }

    public void setTestService(TestService testService) {
        this.testService = testService;
    }

    public QuestionService getQuestionService() {
        return questionService;
    }

    public void setQuestionService(QuestionService questionService) {
        this.questionService = questionService;
    }

    public TestQuestionService getTestQuestionService() {
        return testQuestionService;
    }

    public void setTestQuestionService(TestQuestionService testQuestionService) {
        this.testQuestionService = testQuestionService;
    }
}
