package com.tn.medianet.backend.controller;


import com.tn.medianet.backend.dao.UserRepositoryDao;
import com.tn.medianet.backend.model.Candidat;
import com.tn.medianet.backend.model.Reunion;
import com.tn.medianet.backend.services.CandidatService;
import com.tn.medianet.backend.services.ReunionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping(value ="/admin")
@CrossOrigin(origins = "*")
public class ReunionController {

    @Autowired
    private ReunionService reunionService;

    @Autowired
    private CandidatService candidatService;

    @Autowired
    private UserRepositoryDao userRepository;

    //************* All Test***********
    @RequestMapping(value = "/reunion/getAllReunion", method = RequestMethod.GET)
    public ResponseEntity getAllReunion(){
        List<Reunion>reunionList=this.reunionService.getAllReunion();
        if(reunionList!=null){
            return   new ResponseEntity<>(reunionList,HttpStatus.OK);
        }
        return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }



    @PutMapping(value="/reunion/addreunion/{id}")
    public ResponseEntity getResult(@PathVariable("id") int id_c, @RequestBody Reunion reunion) {
            int id=189;

           Optional<com.tn.medianet.backend.model.User>Optionnaluser=this.userRepository.
                   findById(new Long(id));

        com.tn.medianet.backend.model.User user=Optionnaluser.get();

           Candidat candidat=this.candidatService.getCandiatById(id_c);

           reunion.setCandidat(candidat);
           reunion.setUser(user);

            System.out.println("user "+user.toString());
        System.out.println("candidatat "+candidat.toString());
        System.out.println("**************Reunion after save  "+reunion.toString()+"*****************");

        System.out.println("*******Date heure start  "+reunion.getStartTime().getHours()+" minute"+reunion.getStartTime().getMinutes()+" month ***********"+
                reunion.getStartTime().getMonth());
        




        if(reunion!=null){
            int min=10;
            int max=1500;
            double x = (int)(Math.random()*((max-min)+1))+min;

            reunion.setId(String.valueOf(x));
            reunion=this.reunionService.saveReunion(reunion);
            return new ResponseEntity<>(reunion,HttpStatus.OK);
        }

       // this.reunionService.saveReunion(reunion);
        //if()


        return new ResponseEntity<>(HttpStatus.NOT_FOUND);

    }
}
