package com.tn.medianet.backend.controller;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.tn.medianet.backend.model.Question;
import com.tn.medianet.backend.model.Test;
import com.tn.medianet.backend.model.modelQuiz.Option;
import com.tn.medianet.backend.model.modelQuiz.QuestionQuiz;
import com.tn.medianet.backend.services.Interfaces.SimulerTest;
import com.tn.medianet.backend.services.SimulerTestService;
import com.tn.medianet.backend.services.TestQuestionService;
import com.tn.medianet.backend.services.TestService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.configurationprocessor.json.JSONObject;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping(value ="/admin")
@CrossOrigin(origins = "*")
public class SimulerTestController {


    @Autowired
    private SimulerTestService simulerTestService;


    @Autowired
    private TestService testService;

    @Autowired
    private TestQuestionService testQuestionService;

    @GetMapping(value="/simulerTest/getQuestionByIdTest/{id}")
    public ResponseEntity getQuestionTestById(@PathVariable("id") int id) {
        List<Question>listq=new ArrayList<Question>();

        listq=this.simulerTestService.getAllTestInfoByIdTest(id);
        if(listq.size()>0){
            return new ResponseEntity<>(listq,HttpStatus.OK);

        }
        return new ResponseEntity<>(HttpStatus.NOT_FOUND);


    }




    @PutMapping(value="/simulerTest/resultatTest/{id}")
    public ResponseEntity getResult(@PathVariable("id") int id, @RequestBody List<QuestionQuiz> listQuiz) {
        this.afficheText("id Test is "+id);
        int nb =0;
        String resultatJson;
        for(int i=0;i<listQuiz.size();i++){
            this.afficheText("Question quiz is \t ["+listQuiz.get(i).toString() );

            List<Option> optionList=listQuiz.get(i).getOptions();

            for(int j=0;j<optionList.size();j++){
                Option option=optionList.get(j);
                if(option.getAnswer().booleanValue()==option.getSelected().booleanValue() && option.getAnswer().booleanValue()==true){
                    nb++;
                }
            }

        }
        Gson prettyGson = new GsonBuilder().setPrettyPrinting().create();

        resultatJson=prettyGson.toJson(listQuiz);

        Float score=000.000f;
        score=(float)(nb*100)/listQuiz.size();

        this.afficheText("le score prevu est "+score);

        Test test=new Test();

        Optional<Test>optionalTest=this.testService.getTestById(id);
        test=optionalTest.get();

        test.setScore(score);
        test.setResultat(resultatJson);
        test.setEtatTest(1);
        test.setDateEvaluation(new Date());

        this.testService.Save(test);
        this.afficheText("Votre test deviennet "+test.toString()+" \n resulat"+test.getResultat());
        if(test.getEtatTest()>0){
            return new ResponseEntity<>(true,HttpStatus.OK);

        }else {
            return new ResponseEntity<>(false, HttpStatus.NOT_FOUND);
        }

    }

    @GetMapping(value="/simulerTest/getQuestionQuizByIdTest/{id}")
    public ResponseEntity getQuestionQuizTestById(@PathVariable("id") int id) {
        List<QuestionQuiz>listq=new ArrayList<QuestionQuiz>();

        listq=this.simulerTestService.getAllTestInfoQuizByIdTest(id);

        if(listq.size()>0){
            return new ResponseEntity<>(listq,HttpStatus.OK);

        }
        return new ResponseEntity<>(HttpStatus.NOT_FOUND);

    }


    public SimulerTestService getSimulerTestService() {
        return simulerTestService;
    }

    public void setSimulerTestService(SimulerTestService simulerTestService) {
        this.simulerTestService = simulerTestService;
    }

    public void afficheText(String msg)
    {
        System.out.println(msg);
    }

}
