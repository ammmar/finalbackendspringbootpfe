package com.tn.medianet.backend.dao;

import com.tn.medianet.backend.model.Candidat;
import com.tn.medianet.backend.model.EcoleDto;
import com.tn.medianet.backend.model.EcoleModel;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface CandiatRepository extends JpaRepository<Candidat,Integer> {

    List<Candidat> findAll();

    @Query(value = "SELECT  DISTINCT (c.ecole),count(c.ecole) as co  from candidat as c GROUP by c.ecole ORDER by co desc",nativeQuery = true)
        List<EcoleDto>getAllStatecoleCount();

    @Query(value="call getNumberCandidatByMonth(:month)",nativeQuery = true)
    int getNumberCandidatByMonth (@Param("month") String month);


}
