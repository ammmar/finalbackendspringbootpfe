package com.tn.medianet.backend.dao;
import com.tn.medianet.backend.model.Choix;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.security.core.parameters.P;
import org.springframework.stereotype.Repository;

import java.util.List;

public interface ChoixRepository extends  JpaRepository<Choix,Integer> {
    List<Choix> findAll();

    @Modifying
    @Query(value = "UPDATE choix ch SET ch.reponse=:rep WHERE( (ch.choix NOT LIKE :choix) )",nativeQuery = true)
    int  updateReponse(@Param("choix")String choix, @Param("rep") int rep);
}
