package com.tn.medianet.backend.dao;

import com.tn.medianet.backend.model.Question;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface QuestionRepository extends JpaRepository<Question,Integer> {
    List<Question>findAll();

    //SELECT  DISTINCT id_question,desgn_question,temps_question,temps_question,unite  FROM question

    @Query(
            value = " SELECT   *  FROM question where etat_origin=1",
            nativeQuery = true)
    List<Question>getAllQuestionDistinct();
    @Query(
            value = "SELECT   DISTINCT(desgn_question)  FROM question",
            nativeQuery = true)
    List<String>getAllQuestionName();


}
