package com.tn.medianet.backend.dao;

import com.tn.medianet.backend.model.Reunion;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface ReunionRepository extends JpaRepository<Reunion,Integer> {

    List<Reunion>findAll();
}

