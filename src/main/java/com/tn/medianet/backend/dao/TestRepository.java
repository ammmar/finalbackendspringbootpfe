package com.tn.medianet.backend.dao;


import com.tn.medianet.backend.model.Test;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

@Repository
public interface TestRepository extends  JpaRepository<Test,Integer>{

    public List<Test> findAll();

    @Query(value="call NbrTestSuccess()",nativeQuery = true)
    int NbrTestSuccess();

    @Query(value="call NbrTestFailed()",nativeQuery = true)
    int NbrTestFailed();

    @Query(value="call TestNotPassed()",nativeQuery = true)
    int TestNotPassed();


    @Query(
            value = " SELECT   *  FROM test where etat_origin=1",
            nativeQuery = true)
    List<Test>getAllTestDistinct();











}
