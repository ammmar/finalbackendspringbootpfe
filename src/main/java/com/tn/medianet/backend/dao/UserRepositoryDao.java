package com.tn.medianet.backend.dao;

import com.tn.medianet.backend.model.User;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

/**
 * Created by BMH
 */
public interface UserRepositoryDao extends JpaRepository<User, Long> {
    User findByUsername(String username);
    void delete(User user);
    List<User> findAll();
    User save(User user);
    List<User> findAllById(Long id);
}
