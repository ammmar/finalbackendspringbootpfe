package com.tn.medianet.backend.model;

import com.fasterxml.jackson.annotation.JsonIgnore;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Arrays;
import java.util.Date;
import java.util.List;


/**
 * The persistent class for the candidat database table.
 * 
 */
@Entity
@Table(name="candidat")
@NamedQuery(name="Candidat.findAll", query="SELECT c FROM Candidat c")
public class Candidat implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="id_candidat", unique=true, nullable=false)
	private int idCandidat;

	@Lob
	private byte[] cv;

	@Column(nullable=false, length=200)
	private String ecole;

	@Column(nullable=false, length=50)
	private String email;

	@Column(nullable=false, length=200)
	private String experience;

	@Column(nullable=false)
	private int niveau;

	@Column(nullable=false, length=20)
	private String nom;


	@Column(nullable=false, length=100)
	private String month;

	@Column(nullable=false, length=20)
	private String prenom;

	@Column(nullable=false, length=20)
	private String profil;

	@Column(nullable=false, length=20)
	private String tel;

	@Temporal(TemporalType.DATE)
	@Column(name="date_debot", nullable=false)
	private Date dateDebot;

	@Temporal(TemporalType.DATE)
	@Column(name="date_Evaluation", nullable=false)
	private Date date_Evaluation;

	//bi-directional many-to-one association to CandidatReponseChoix
	@JsonIgnore
	@OneToMany(mappedBy="candidat")
	private List<CandidatReponseChoix> candidatReponseChoixs;

	//bi-directional many-to-one association to
	@JsonIgnore
	@OneToMany(mappedBy="candidat")
	private List<Test> tests;


	//bi-directional many-to-one association to Reunion
	@OneToMany(mappedBy="candidat")
	private List<Reunion> reunions;


	public Candidat() {
	}

	public int getIdCandidat() {
		return this.idCandidat;
	}

	public void setIdCandidat(int idCandidat) {
		this.idCandidat = idCandidat;
	}

	public byte[] getCv() {
		return this.cv;
	}

	public void setCv(byte[] cv) {
		this.cv = cv;
	}

	public String getEcole() {
		return this.ecole;
	}

	public void setEcole(String ecole) {
		this.ecole = ecole;
	}

	public String getEmail() {
		return this.email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getExperience() {
		return this.experience;
	}

	public void setExperience(String experience) {
		this.experience = experience;
	}

	public int getNiveau() {
		return this.niveau;
	}

	public void setNiveau(int niveau) {
		this.niveau = niveau;
	}

	public String getNom() {
		return this.nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}



	public String getPrenom() {
		return this.prenom;
	}

	public void setPrenom(String prenom) {
		this.prenom = prenom;
	}

	public String getTel() {
		return this.tel;
	}

	public void setTel(String tel) {
		this.tel = tel;
	}

	public List<CandidatReponseChoix> getCandidatReponseChoixs() {
		return this.candidatReponseChoixs;
	}

	public void setCandidatReponseChoixs(List<CandidatReponseChoix> candidatReponseChoixs) {
		this.candidatReponseChoixs = candidatReponseChoixs;
	}

	public CandidatReponseChoix addCandidatReponseChoix(CandidatReponseChoix candidatReponseChoix) {
		getCandidatReponseChoixs().add(candidatReponseChoix);
		candidatReponseChoix.setCandidat(this);

		return candidatReponseChoix;
	}

	public CandidatReponseChoix removeCandidatReponseChoix(CandidatReponseChoix candidatReponseChoix) {
		getCandidatReponseChoixs().remove(candidatReponseChoix);
		candidatReponseChoix.setCandidat(null);

		return candidatReponseChoix;
	}


	public List<Test> getTests() {
		return this.tests;
	}

	public void setTests(List<Test> tests) {
		this.tests = tests;
	}

	public Test addTest(Test test) {
		getTests().add(test);
		test.setCandidat(this);

		return test;
	}

	public Test removeTest(Test test) {
		getTests().remove(test);
		test.setCandidat(null);

		return test;
	}

	public String getProfil() {
		return profil;
	}

	public void setProfil(String profil) {
		this.profil = profil;
	}

	public Date getDateDebot() {
		return dateDebot;
	}

	public void setDateDebot(Date dateDebot) {
		this.dateDebot = dateDebot;
	}

	public Date getDate_Evaluation() {
		return date_Evaluation;
	}

	public void setDate_Evaluation(Date date_Evaluation) {
		this.date_Evaluation = date_Evaluation;
	}

	public String getMonth() {
		return month;
	}

	public void setMonth(String month) {
		this.month = month;
	}

	@Override
	public String toString() {
		return "Candidat{" +
				"idCandidat=" + idCandidat +
				", ecole='" + ecole + '\'' +
				", email='" + email + '\'' +
				", experience='" + experience + '\'' +
				", niveau=" + niveau +
				", nom='" + nom + '\'' +
				", month='" + month + '\'' +
				", prenom='" + prenom + '\'' +
				", profil='" + profil + '\'' +
				", tel='" + tel + '\'' +
				", dateDebot=" + dateDebot +
				", date_Evaluation=" + date_Evaluation +
				'}';
	}
}