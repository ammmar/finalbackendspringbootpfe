package com.tn.medianet.backend.model;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;


/**
 * The persistent class for the candidat_reponse_choix database table.
 * 
 */
@Entity
@Table(name="candidat_reponse_choix")
@NamedQuery(name="CandidatReponseChoix.findAll", query="SELECT c FROM CandidatReponseChoix c")
public class CandidatReponseChoix implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="id_crc", unique=true, nullable=false)
	private int idCrc;

	@Temporal(TemporalType.DATE)
	@Column(nullable=false)
	private Date date;

	//bi-directional many-to-one association to Candidat
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="id_candidat", nullable=false)
	private Candidat candidat;

	//bi-directional many-to-one association to Choix
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="id_choix", nullable=false)
	private Choix choix;

	//bi-directional many-to-one association to ReponseCandidat
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="id_reponse", nullable=false)
	private ReponseCandidat reponseCandidat;

	public CandidatReponseChoix() {
	}

	public int getIdCrc() {
		return this.idCrc;
	}

	public void setIdCrc(int idCrc) {
		this.idCrc = idCrc;
	}

	public Date getDate() {
		return this.date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public Candidat getCandidat() {
		return this.candidat;
	}

	public void setCandidat(Candidat candidat) {
		this.candidat = candidat;
	}

	public Choix getChoix() {
		return this.choix;
	}

	public void setChoix(Choix choix) {
		this.choix = choix;
	}

	public ReponseCandidat getReponseCandidat() {
		return this.reponseCandidat;
	}

	public void setReponseCandidat(ReponseCandidat reponseCandidat) {
		this.reponseCandidat = reponseCandidat;
	}

}