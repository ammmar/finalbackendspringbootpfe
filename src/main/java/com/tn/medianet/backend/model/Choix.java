package com.tn.medianet.backend.model;

import com.fasterxml.jackson.annotation.JsonIgnore;

import java.io.Serializable;
import javax.persistence.*;
import java.util.List;


/**
 * The persistent class for the choix database table.
 * 
 */
@Entity
@Table(name="choix")
@NamedQuery(name="Choix.findAll", query="SELECT c FROM Choix c")
public class Choix implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name="id_choix", unique=true, nullable=false)
	private int idChoix;

	@Lob
	@Column(nullable=false)
	private String choix;

	@Column(name="reponse", nullable=false)
	private int reponse;
	//bi-directional many-to-one association to CandidatReponseChoix
	@JsonIgnore
	@OneToMany(mappedBy="choix")
	private List<CandidatReponseChoix> candidatReponseChoixs;

	//bi-directional many-to-one association to QuestionQuiz
	@JsonIgnore
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="id_question", nullable=false)
	private Question question;

	public Choix() {
	}

	public int getIdChoix() {
		return this.idChoix;
	}

	public void setIdChoix(int idChoix) {
		this.idChoix = idChoix;
	}

	public String getChoix() {
		return this.choix;
	}

	public void setChoix(String choix) {
		this.choix = choix;
	}



	public List<CandidatReponseChoix> getCandidatReponseChoixs() {
		return this.candidatReponseChoixs;
	}

	public void setCandidatReponseChoixs(List<CandidatReponseChoix> candidatReponseChoixs) {
		this.candidatReponseChoixs = candidatReponseChoixs;
	}

	public CandidatReponseChoix addCandidatReponseChoix(CandidatReponseChoix candidatReponseChoix) {
		getCandidatReponseChoixs().add(candidatReponseChoix);
		candidatReponseChoix.setChoix(this);

		return candidatReponseChoix;
	}

	public CandidatReponseChoix removeCandidatReponseChoix(CandidatReponseChoix candidatReponseChoix) {
		getCandidatReponseChoixs().remove(candidatReponseChoix);
		candidatReponseChoix.setChoix(null);

		return candidatReponseChoix;
	}

	public Question getQuestion() {
		return this.question;
	}

	public void setQuestion(Question question) {
		this.question = question;
	}

	public int getReponse() {
		return reponse;
	}

	public void setReponse(int reponse) {
		this.reponse = reponse;
	}


	@Override
	public String toString() {
		return "Choix{" +
				"idChoix=" + idChoix +
				", choix='" + choix + '\'' +
				", reponse=" + reponse +
				'}'+"question"+question.toString();
	}
}