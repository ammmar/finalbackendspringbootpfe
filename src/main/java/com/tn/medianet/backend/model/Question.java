package com.tn.medianet.backend.model;

import com.fasterxml.jackson.annotation.JsonIgnore;

import java.io.Serializable;
import javax.persistence.*;
import java.util.List;


/**
 * The persistent class for the question database table.
 * 
 */
@Entity
@Table(name="question")
@NamedQuery(name="Question.findAll", query="SELECT q FROM Question q")
public class Question implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name="id_question", unique=true, nullable=false)
	private int idQuestion;

	@Lob
	@Column(name="desgn_question", nullable=false)
	private String desgnQuestion;

	@Column(name="temps_question", nullable=false)
	private int tempsQuestion;

	@Column(nullable=false, length=100)
	private String unite;

	@Column(name="etat_origin", nullable=false)
	private int etatOrigin;


	@Transient
	private Boolean disabled;

	@Transient
	private Boolean  checked;

	//bi-directional many-to-one association to Choix

	@OneToMany(mappedBy="question")
	private List<Choix> choixs;

	//bi-directional many-to-one association to Testquestion
	@JsonIgnore
	@OneToMany(mappedBy="question")
	private List<Testquestion> testquestions;

	@Column(nullable=false, length=100)
	private String theme;

	public Question() {
	}

	public int getIdQuestion() {
		return this.idQuestion;
	}

	public void setIdQuestion(int idQuestion) {
		this.idQuestion = idQuestion;
	}

	public String getDesgnQuestion() {
		return this.desgnQuestion;
	}

	public void setDesgnQuestion(String desgnQuestion) {
		this.desgnQuestion = desgnQuestion;
	}

	public int getTempsQuestion() {
		return this.tempsQuestion;
	}

	public void setTempsQuestion(int tempsQuestion) {
		this.tempsQuestion = tempsQuestion;
	}

	public String getUnite() {
		return this.unite;
	}

	public void setUnite(String unite) {
		this.unite = unite;
	}

	public List<Choix> getChoixs() {
		return this.choixs;
	}

	public void setChoixs(List<Choix> choixs) {
		this.choixs = choixs;
	}

	public Choix addChoix(Choix choix) {
		getChoixs().add(choix);
		choix.setQuestion(this);

		return choix;
	}

	public Choix removeChoix(Choix choix) {
		getChoixs().remove(choix);
		choix.setQuestion(null);

		return choix;
	}

	public List<Testquestion> getTestquestions() {
		return this.testquestions;
	}

	public void setTestquestions(List<Testquestion> testquestions) {
		this.testquestions = testquestions;
	}

	public Testquestion addTestquestion(Testquestion testquestion) {
		getTestquestions().add(testquestion);
		testquestion.setQuestion(this);

		return testquestion;
	}

	public Testquestion removeTestquestion(Testquestion testquestion) {
		getTestquestions().remove(testquestion);
		testquestion.setQuestion(null);

		return testquestion;
	}

	public Boolean getDisabled() {
		return disabled;
	}

	public void setDisabled(Boolean disabled) {
		this.disabled = disabled;
	}

	public Boolean getChecked() {
		return checked;
	}

	public void setChecked(Boolean checked) {
		this.checked = checked;
	}

	public String getTheme() {
		return theme;
	}

	public void setTheme(String theme) {
		this.theme = theme;
	}

	public int getEtatOrigin() {
		return etatOrigin;
	}

	public void setEtatOrigin(int etatOrigin) {
		this.etatOrigin = etatOrigin;
	}

	@Override
	public String toString() {
		return "Question{" +
				"idQuestion=" + idQuestion +
				", desgnQuestion='" + desgnQuestion + '\'' +
				", tempsQuestion=" + tempsQuestion +
				", unite='" + unite + '\'' +
				", disabled=" + disabled +
				", checked=" + checked +
				'}';
	}
}