package com.tn.medianet.backend.model;

import java.io.Serializable;
import javax.persistence.*;
import java.util.List;


/**
 * The persistent class for the reponse_candidat database table.
 * 
 */
@Entity
@Table(name="reponse_candidat")
@NamedQuery(name="ReponseCandidat.findAll", query="SELECT r FROM ReponseCandidat r")
public class ReponseCandidat implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="id_reponse", unique=true, nullable=false)
	private int idReponse;

	@Column(nullable=false)
	private int reponse;

	//bi-directional many-to-one association to CandidatReponseChoix
	@OneToMany(mappedBy="reponseCandidat")
	private List<CandidatReponseChoix> candidatReponseChoixs;

	public ReponseCandidat() {
	}

	public int getIdReponse() {
		return this.idReponse;
	}

	public void setIdReponse(int idReponse) {
		this.idReponse = idReponse;
	}

	public int getReponse() {
		return this.reponse;
	}

	public void setReponse(int reponse) {
		this.reponse = reponse;
	}

	public List<CandidatReponseChoix> getCandidatReponseChoixs() {
		return this.candidatReponseChoixs;
	}

	public void setCandidatReponseChoixs(List<CandidatReponseChoix> candidatReponseChoixs) {
		this.candidatReponseChoixs = candidatReponseChoixs;
	}

	public CandidatReponseChoix addCandidatReponseChoix(CandidatReponseChoix candidatReponseChoix) {
		getCandidatReponseChoixs().add(candidatReponseChoix);
		candidatReponseChoix.setReponseCandidat(this);

		return candidatReponseChoix;
	}

	public CandidatReponseChoix removeCandidatReponseChoix(CandidatReponseChoix candidatReponseChoix) {
		getCandidatReponseChoixs().remove(candidatReponseChoix);
		candidatReponseChoix.setReponseCandidat(null);

		return candidatReponseChoix;
	}

}