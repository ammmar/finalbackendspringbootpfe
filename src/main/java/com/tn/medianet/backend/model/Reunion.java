package com.tn.medianet.backend.model;

import com.fasterxml.jackson.annotation.JsonIgnore;

import java.io.Serializable;
import javax.persistence.*;
import java.sql.Timestamp;
import java.util.Date;


/**
 * The persistent class for the reunion database table.
 * 
 */
@Entity
@Table(name="reunion")
@NamedQuery(name="Reunion.findAll", query="SELECT r FROM Reunion r")
public class Reunion implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(unique=true, nullable=false)
	private String id;

	@Column(name="end_time", nullable=false)
	private Timestamp EndTime;


	@Column(name="is_all_day", nullable=false)
	private byte IsAllDay;

	@Column(name="start_time", nullable=false)
	private Timestamp StartTime;



	@Column(nullable=false, length=200)
	private String subject;


	//bi-directional many-to-one association to Candidat
	@JsonIgnore
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="id_candidat", nullable=false)
	private Candidat candidat;

	//bi-directional many-to-one association to User
	@JsonIgnore
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="id_user", nullable=false)
	private User user;

	public Reunion() {
	}

	public String getId() {
		return this.id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public Timestamp getEndTime() {
		return EndTime;
	}

	public void setEndTime(Timestamp endTime) {
		EndTime = endTime;
	}

	public byte getIsAllDay() {
		return IsAllDay;
	}

	public void setIsAllDay(byte isAllDay) {
		IsAllDay = isAllDay;
	}

	public Timestamp getStartTime() {
		return StartTime;
	}

	public void setStartTime(Timestamp startTime) {
		StartTime = startTime;
	}

	public String getSubject() {
		return subject;
	}

	public void setSubject(String subject) {
		this.subject = subject;
	}

	public Candidat getCandidat() {
		return this.candidat;
	}

	public void setCandidat(Candidat candidat) {
		this.candidat = candidat;
	}

	public User getUser() {
		return this.user;
	}

	public void setUser(User user) {
		this.user = user;
	}


	@Override
	public String toString() {
		return "Reunion{" +
				"id='" + id + '\'' +
				", endTime=" + EndTime +
				", isAllDay=" + IsAllDay +
				", startTime=" + StartTime +
				", subject='" + subject + '\'' +
				"user"+this.user.toString()+
				"candidat"+this.candidat.toString() +

		'}';
	}
}