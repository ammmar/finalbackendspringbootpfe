package com.tn.medianet.backend.model;

import com.fasterxml.jackson.annotation.JsonIgnore;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Arrays;
import java.util.Date;
import java.util.List;


/**
 * The persistent class for the test database table.
 * 
 */
@Entity
@Table(name="test")
@NamedQuery(name="Test.findAll", query="SELECT t FROM Test t")
public class Test implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name="id_test", unique=true, nullable=false)
	private int idTest;

	@Temporal(TemporalType.DATE)
	@Column(name="date_debut", nullable=false)
	private Date dateDebut;

	@Temporal(TemporalType.DATE)
	@Column(name="date_evaluation", nullable=false)
	private Date dateEvaluation;

	@Temporal(TemporalType.DATE)
	@Column(name="date_fin", nullable=false)
	private Date dateFin;

	@Lob
	@Column(nullable=false)
	private String description;

	@Column(name="etat_test", nullable=false)
	private int etatTest;




	@Lob
	private byte[] logo;

	@Column(name="etat_origin", nullable=false)
	private int etatOrigin;

	@Column(nullable=false, length=100)
	private String theme;

	@Column(name="type_test", nullable=false, length=100)
	private String typeTest;

	@Lob
	@Column(nullable=false)
	private String resultat;

	@Column(nullable=false)
	private float score;


	@Transient
	private String nomcandidat;

	@Transient
	private String etatTestInfo;

	@Transient
	private int id_candidat;

	@Transient
	 private String  prenomcandidat;
	//bi-directional many-to-one association to Candidat


	@Transient
	private String  emailcandidat;

	@Transient
	private Integer durationTest;

	@Transient
	private String numTel;
	@JsonIgnore
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="id_candidat", nullable=false)
	private Candidat candidat;

	//bi-directional many-to-one association to Testquestion
	@JsonIgnore
	@OneToMany(mappedBy="test")
	private List<Testquestion> testquestions;




	public Test() {
	}

	public Test(Date dateDebut, Date dateFin, String description, int etatTest, float score, String theme, String typeTest){
		this.dateFin = dateFin;
		this.description = description;
		this.etatTest = etatTest;
		this.score = score;
		this.theme = theme;
		this.typeTest = typeTest;
	}

	public int getIdTest() {
		return idTest;
	}

	public void setIdTest(int idTest) {
		this.idTest = idTest;
	}

	public Date getDateDebut() {
		return dateDebut;
	}

	public void setDateDebut(Date dateDebut) {
		this.dateDebut = dateDebut;
	}

	public Date getDateFin() {
		return dateFin;
	}

	public void setDateFin(Date dateFin) {
		this.dateFin = dateFin;
	}

	public Date getDateEvaluation() {
		return dateEvaluation;
	}

	public void setDateEvaluation(Date dateEvaluation) {
		this.dateEvaluation = dateEvaluation;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public byte[] getLogo() {
		return logo;
	}

	public void setLogo(byte[] logo) {
		this.logo = logo;
	}

	public String getTheme() {
		return theme;
	}

	public void setTheme(String theme) {
		this.theme = theme;
	}

	public String getTypeTest() {
		return typeTest;
	}

	public void setTypeTest(String typeTest) {
		this.typeTest = typeTest;
	}

	public Candidat getCandidat() {
		return candidat;
	}

	public void setCandidat(Candidat candidat) {
		this.candidat = candidat;
	}

	public List<Testquestion> getTestquestions() {
		return testquestions;
	}

	public void setTestquestions(List<Testquestion> testquestions) {
		this.testquestions = testquestions;
	}

	public int getEtatTest() {
		return etatTest;
	}

	public void setEtatTest(int etatTest) {
		this.etatTest = etatTest;
	}






	public int getId_candidat() {
		return id_candidat;
	}

	public void setId_candidat(int id_candidat) {
		this.id_candidat = id_candidat;
	}

	public String getNomcandidat() {
		return nomcandidat;
	}

	public void setNomcandidat(String nomcandidat) {
		this.nomcandidat = nomcandidat;
	}

	public String getPrenomcandidat() {
		return prenomcandidat;
	}

	public String getEtatTestInfo() {
		return etatTestInfo;
	}

	public void setEtatTestInfo(String etatTestInfo) {
		this.etatTestInfo = etatTestInfo;
	}

	public void setPrenomcandidat(String prenomcandidat) {
		this.prenomcandidat = prenomcandidat;
	}

	public String getNumTel() {
		return numTel;
	}

	public void setNumTel(String numTel) {
		this.numTel = numTel;
	}

	public String getResultat() {
		return resultat;
	}

	public void setResultat(String resultat) {
		this.resultat = resultat;
	}

	public float getScore() {
		return score;
	}

	public void setScore(float score) {
		this.score = score;
	}

	public Integer getDurationTest() {
		return durationTest;
	}

	public void setDurationTest(Integer durationTest) {
		this.durationTest = durationTest;
	}

	public String getEmailcandidat() {
		return emailcandidat;
	}

	public void setEmailcandidat(String emailcandidat) {
		this.emailcandidat = emailcandidat;
	}

	public int getEtatOrigin() {
		return etatOrigin;
	}

	public void setEtatOrigin(int etatOrigin) {
		this.etatOrigin = etatOrigin;
	}

	@Override
	public String toString() {
		return "Test{" +
				"idTest=" + idTest +
				", dateDebut=" + dateDebut +
				", dateFin=" + dateFin +
				", dateEvaluation=" + dateEvaluation +
				", description='" + description + '\'' +
				", logo=" + Arrays.toString(logo) +
				", theme='" + theme + '\'' +
				", typeTest='" + typeTest + '\'' +
				", etatTest=" + etatTest +
				", score=" + score +
				'}';
	}


}