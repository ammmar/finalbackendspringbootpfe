package com.tn.medianet.backend.model;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;


/**
 * The persistent class for the testquestion database table.
 * 
 */
@Entity
@Table(name="testquestion")
@NamedQuery(name="Testquestion.findAll", query="SELECT t FROM Testquestion t")
public class Testquestion implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name="id_test_question", unique=true, nullable=false)
	private int idTestQuestion;

	//bi-directional many-to-one association to QuestionQuiz
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="id_question", nullable=false)
	private Question question;

	//bi-directional many-to-one association to Test
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="id_test", nullable=false)
	private Test test;

	public Testquestion() {
	}

	public int getIdTestQuestion() {
		return this.idTestQuestion;
	}

	public void setIdTestQuestion(int idTestQuestion) {
		this.idTestQuestion = idTestQuestion;
	}

	public Question getQuestion() {
		return this.question;
	}

	public void setQuestion(Question question) {
		this.question = question;
	}

	public Test getTest() {
		return this.test;
	}

	public void setTest(Test test) {
		this.test = test;
	}

	@Override
	public String toString() {
		return "Testquestion{" +
				"idTestQuestion=" + idTestQuestion +
				", question=" + question +
				", test=" + test +
				'}';
	}
}