package com.tn.medianet.backend.model.modelQuiz;

import com.sun.org.apache.xpath.internal.operations.Bool;

public class Option {
    private int id;
    private int questionId;
    private   String name ;
    private Boolean answer;
    private Boolean selected;

    public Option(int id, int questionId, String name, Boolean isAnswer) {
        this.id = id;
        this.questionId = questionId;
        this.name = name;
        this.answer = isAnswer;
    }


    public Option() {

    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getQuestionId() {
        return questionId;
    }

    public void setQuestionId(int questionId) {
        this.questionId = questionId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Boolean getAnswer() {
        return answer;
    }

    public void setAnswer(Boolean answer) {
        this.answer = answer;
    }

    public Boolean getSelected() {
        return selected;
    }

    public void setSelected(Boolean selected) {
        this.selected = selected;
    }


    @Override
    public String toString() {
        return "Option{" +
                "id=" + id +
                ", questionId=" + questionId +
                ", name='" + name + '\'' +
                ", answer=" + answer +
                ", selected=" + selected +
                '}';
    }
}
