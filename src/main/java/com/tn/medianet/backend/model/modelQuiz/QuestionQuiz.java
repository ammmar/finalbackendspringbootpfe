package com.tn.medianet.backend.model.modelQuiz;

import java.util.List;

public class QuestionQuiz {

/*
   questionquiz = {
            "id": q["idQuestion"], "name": q["desgnQuestion"], "questionTypeId": 1,
            "options": options
          }
 */

private int id;
private String name;
private int questionTypeId;
private List<Option> options;
    public QuestionQuiz(){

    }

    public QuestionQuiz(int id, String name, int questionTypeId, List<Option> options) {
        this.id = id;
        this.name = name;
        this.questionTypeId = questionTypeId;
        this.options = options;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getQuestionTypeId() {
        return questionTypeId;
    }

    public void setQuestionTypeId(int questionTypeId) {
        this.questionTypeId = questionTypeId;
    }

    public List<Option> getOptions() {
        return options;
    }

    public void setOptions(List<Option> options) {
        this.options = options;
    }

    @Override
    public String toString() {
        return "{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", questionTypeId=" + questionTypeId +
                ", options=" + options +
                '}';
    }
}
