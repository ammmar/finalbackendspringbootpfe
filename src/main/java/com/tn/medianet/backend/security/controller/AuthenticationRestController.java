package com.tn.medianet.backend.security.controller;

import com.tn.medianet.backend.model.Message;
import com.tn.medianet.backend.modele.security.Authority;
import com.tn.medianet.backend.modele.security.AuthorityName;
import com.tn.medianet.backend.modele.security.User;
import com.tn.medianet.backend.security.JwtAuthenticationRequest;
import com.tn.medianet.backend.security.JwtTokenUtil;
import com.tn.medianet.backend.security.JwtUser;
import com.tn.medianet.backend.security.repository.AuthorityInterface;
import com.tn.medianet.backend.security.repository.UserRepository;
import com.tn.medianet.backend.security.service.JwtAuthenticationResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.DisabledException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Optional;

@RestController
@RequestMapping(value ="/user")
@CrossOrigin(origins = "*")
public class AuthenticationRestController {

    @Value("${jwt.header}")
    private String tokenHeader;

    @Autowired
    private AuthenticationManager authenticationManager;

    @Autowired
    private JwtTokenUtil jwtTokenUtil;

    @Autowired
    @Qualifier("jwtUserDetailsService")
    private UserDetailsService userDetailsService;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private AuthorityInterface authorityRepository;

    @Autowired
    private PasswordEncoder passwordEncoder;
@PostMapping("/auth")
    public ResponseEntity<?> createAuthenticationToken(@RequestBody JwtAuthenticationRequest authenticationRequest) throws AuthenticationException {

        System.out.println("user name password "+authenticationRequest.getUsername()+"/"+ authenticationRequest.getPassword());
        authenticate(authenticationRequest.getUsername(), authenticationRequest.getPassword());

        // Reload password post-security so we can generate the token
        final UserDetails userDetails = userDetailsService.loadUserByUsername(authenticationRequest.getUsername());
        if(userDetails.getUsername().isEmpty())
        {
            System.out.println("not found");
            return ResponseEntity.badRequest().body(new Message("Not Found"));
        }

        final String token = jwtTokenUtil.generateToken(userDetails);

        // Return the token
        return ResponseEntity.ok(new JwtAuthenticationResponse(token));
    }

    @RequestMapping(value = "${jwt.route.authentication.refresh}", method = RequestMethod.GET)
    public ResponseEntity<?> refreshAndGetAuthenticationToken(HttpServletRequest request) {
        String authToken = request.getHeader(tokenHeader);
        final String token = authToken.substring(7);
        String username = jwtTokenUtil.getUsernameFromToken(token);
        JwtUser user = (JwtUser) userDetailsService.loadUserByUsername(username);

        if (jwtTokenUtil.canTokenBeRefreshed(token, user.getLastPasswordResetDate())) {
            String refreshedToken = jwtTokenUtil.refreshToken(token);
            return ResponseEntity.ok(new JwtAuthenticationResponse(refreshedToken));
        } else {
            return ResponseEntity.badRequest().body(null);
        }
    }

    @ExceptionHandler({AuthenticationException.class})
    public ResponseEntity<String> handleAuthenticationException(AuthenticationException e) {
        return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body(e.getMessage());
    }

    /**
     * Authenticates the user. If something is wrong, an {@link AuthenticationException} will be thrown
     */

    @RequestMapping(value = "/getAllUser", method = RequestMethod.GET)
    public ResponseEntity getAllCandidat() {

        List<User> listuser = this.userRepository.findAll();


        if (listuser != null) {
            if (listuser.size() > 0) {
                for(int i=0;i<listuser.size();i++){
                   System.out.println("user "+listuser.get(i).toString() +"\n");
                    //List<Authority>authorityList=listuser.get(i).getAuthorities();
                    /*if(authorityList!=null){
                        if(authorityList.size()>0){
                            System.out.println("user "+listuser.get(i).toString());
                        }
                    }*/

                }
                return new ResponseEntity<>(listuser, HttpStatus.OK);
            } else {
                return new ResponseEntity<>(HttpStatus.NOT_FOUND);
            }

        }

        return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }
    //******add user
    //** Save Or Update
    @PostMapping(value="/adduser")
    public ResponseEntity SaveOrUpdateQuestion(@RequestBody User user) {
        List<Authority>listauthority=new ArrayList<Authority>();
       /*
        authority.setName(AuthorityName.ROLE_RH);
        listauthority.add(authority);

         User userSaved=this.userRepository.save(user);
        userSaved.setAuthorities(listauthority);*/
        //listauthority.add(AuthorityName.ROLE_DEVELOPER);
        //user.setAuthorities(AuthorityName.ROLE_DEVELOPER);

        Authority authority=new Authority();
        authority.setName(AuthorityName.ROLE_ADMIN);
        listauthority.add(authority);
        //authority=  this.authorityRepository.findById(new Long(4)).get();

        List<User>userList=new ArrayList<>();
      //  User userSaved=this.userRepository.save(user);
       // userList.add(user);
        //authority.setUsers(userList);
        System.out.println("user to add is "+user.toString());
        user.setAuthorities(listauthority);
        user.setPassword(passwordEncoder.encode(user.getPassword()));
        User userSaved=this.userRepository.save(user);

        if( userSaved!=null){
            return new ResponseEntity<>(userSaved,HttpStatus.OK);
        }

        return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }
    @PutMapping(value="/updateuser/{id}")
    public ResponseEntity UpdateQuestion(@RequestBody User user,@PathVariable("id")int  id_user) {
        List<Authority>listauthority=new ArrayList<Authority>();

        System.out.println("id user "+id_user);
        Authority authority=new Authority();


        List<User>userList=new ArrayList<>();
        User updateuser=new User();
        if (this.userRepository.findById(new Long(id_user)).isPresent()) {
         Optional<User>   Optionneluser = this.userRepository.findById(new Long(id_user));
         updateuser=Optionneluser.get();
         updateuser.setId(new Long(id_user));
         updateuser.setUsername(user.getUsername());
            updateuser.setPassword(passwordEncoder.encode(user.getPassword()));
            updateuser.setFirstname(user.getFirstname());
            updateuser.setLastname(user.getLastname());

            updateuser.setEmail(user.getEmail());
            updateuser.setEnabled(user.getEnabled());
            updateuser.setLastPasswordResetDate(user.getLastPasswordResetDate());
            updateuser.setUsertel(user.getUsertel());
            updateuser.setUseradresse(user.getUseradresse());
            updateuser.setDisponibilite(user.getDisponibilite());
            updateuser.setPoste(user.getPoste());


        }


        System.out.println("user db  is   "+updateuser.toString());

        System.out.println("user to update   is   "+user.toString());



       // User userSaved=this.userRepository.save(user);

        if( this.userRepository.save(updateuser)!=null){
            return new ResponseEntity<>(user,HttpStatus.OK);
        }

        return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }


    @DeleteMapping(value="/deleteUser/{id}")
    public ResponseEntity DeleteUser(@PathVariable("id") int  id_user) {

        Optional<User> userOptional=this.userRepository.findById(new Long(id_user));
        if(userOptional.isPresent()) {
            this.userRepository.delete(userOptional.get());
            return new ResponseEntity<>(userOptional.get(),HttpStatus.OK);
        }else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }

    }
    @GetMapping(value="/getuserbyid/{id}")
    public ResponseEntity getUserById(@PathVariable("id") int  id_user) {

        Optional<User> userOptional=this.userRepository.findById(new Long(id_user));

        if(this.userRepository.findById(new Long(id_user)).isPresent()) {
            System.out.println("user  "+userOptional.get().toString());
            return new ResponseEntity<>(userOptional.get(),HttpStatus.OK);
        }else {
            System.out.println("user not found   ");

            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }

    }

    private void authenticate(String username, String password) {
        Objects.requireNonNull(username);
        Objects.requireNonNull(password);

        try {
            authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(username, password));
        } catch (DisabledException e) {
            throw new AuthenticationException("User is disabled!", e);
        } catch (BadCredentialsException e) {
            throw new AuthenticationException("Bad credentials!", e);
        }
    }
}