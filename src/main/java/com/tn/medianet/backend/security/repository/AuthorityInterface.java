package com.tn.medianet.backend.security.repository;

import com.tn.medianet.backend.modele.security.Authority;
import com.tn.medianet.backend.modele.security.User;
import org.springframework.data.jpa.repository.JpaRepository;

public interface AuthorityInterface extends JpaRepository<Authority,Long> {
}
