package com.tn.medianet.backend.services;


import com.tn.medianet.backend.dao.CandiatRepository;
import com.tn.medianet.backend.model.Candidat;
import com.tn.medianet.backend.model.EcoleDto;
import com.tn.medianet.backend.model.EcoleModel;
import com.tn.medianet.backend.services.Interfaces.CandidatInterface;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class CandidatService  implements CandidatInterface {

    @Autowired
     private CandiatRepository candidatRepository;
    @Override
    public List<Candidat> getAllCandiat() {
        return  this.candidatRepository.findAll();
    }


    public Candidat save(Candidat c)
    {
        return  this.candidatRepository.save(c);
    }


    public void delete(Candidat c)
    {
         this.candidatRepository.delete(c);

    }

    public Candidat getCandiatById(int id)
    {
        Optional<Candidat> to=this.candidatRepository.findById(id);
        if(to.isPresent())
        return to.get();
        else
            return  null;
    }


    public  List<EcoleModel> getAllStatEcoleCount(){
        List<EcoleModel>listEcoleModel=new ArrayList<EcoleModel>();
        List<Candidat>listCandidat=new ArrayList<Candidat>();
        listCandidat=this.candidatRepository.findAll();
        List<EcoleDto>ecoleDtoList=this.candidatRepository.getAllStatecoleCount();
        int nb;
        for (int i=0;i<ecoleDtoList.size();i++){
            EcoleModel ecoleModel=new EcoleModel();
             nb=0;
             System.out.println("count"+ecoleDtoList.get(i).getCount());
          for(int j=0;j<listCandidat.size();j++){
            if(ecoleDtoList.get(i).getEcole().equals(listCandidat.get(j).getEcole())){
                nb++;
                }
          }
          ecoleModel.setEcole(ecoleDtoList.get(i).getEcole());
           ecoleModel.setCount(nb);
           listEcoleModel.add(ecoleModel);
        }
      return listEcoleModel;


    }





   public int getNumberCandidatByMonth(String month){
        return  this.candidatRepository.getNumberCandidatByMonth(month);
   }


    public CandiatRepository getCandidatRepository() {
        return candidatRepository;
    }

    public void setCandidatRepository(CandiatRepository candidatRepository) {
        this.candidatRepository = candidatRepository;
    }
}
