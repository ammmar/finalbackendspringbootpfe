package com.tn.medianet.backend.services;


import com.tn.medianet.backend.dao.ChoixRepository;
import com.tn.medianet.backend.model.Choix;
import com.tn.medianet.backend.services.Interfaces.ChoixInterafce;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class ChoixService  implements ChoixInterafce {
    @Autowired
    private ChoixRepository choixRepository;


    @Override
    public List<Choix> getAllChoix() {
        return this.choixRepository.findAll();
    }


    //save choix or update

    public Choix saveChoix(Choix choix){
        return this.choixRepository.save(choix);
    }

    //het choix by id choix
    public  Choix getChoixById(int id_choix){
        Optional<Choix> cho= this.choixRepository.findById(id_choix);
        if(cho.isPresent()){
            return  cho.get();
        }
        return  null;
    }

    public int updateChoix(String choix ,int id_question)
    {
     return this.choixRepository.updateReponse(choix,0);
    }






    //accesseur
    public ChoixRepository getChoixRepository() {
        return choixRepository;
    }

    public void setChoixRepository(ChoixRepository choixRepository) {
        this.choixRepository = choixRepository;
    }
}
