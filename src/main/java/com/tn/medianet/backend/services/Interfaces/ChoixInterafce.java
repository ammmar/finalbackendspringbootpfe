package com.tn.medianet.backend.services.Interfaces;

import com.tn.medianet.backend.model.Choix;

import java.util.List;

public interface ChoixInterafce {

    List<Choix> getAllChoix();
}
