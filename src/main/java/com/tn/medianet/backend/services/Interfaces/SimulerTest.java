package com.tn.medianet.backend.services.Interfaces;

import com.tn.medianet.backend.model.Question;
import com.tn.medianet.backend.model.Test;

import java.util.List;

public interface SimulerTest {
  List<Question> getAllTestInfoByIdTest(int id);
}
