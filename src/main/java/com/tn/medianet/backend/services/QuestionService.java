package com.tn.medianet.backend.services;


import com.tn.medianet.backend.dao.ChoixRepository;
import com.tn.medianet.backend.dao.QuestionRepository;
import com.tn.medianet.backend.model.Choix;
import com.tn.medianet.backend.model.Question;
import com.tn.medianet.backend.services.Interfaces.QuestionInterface;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class QuestionService implements  QuestionInterface {


    @Autowired
    private QuestionRepository questionRepository;

    @Autowired
    private ChoixRepository choixRepository;
    @Override
    public List<Question> getAllQuestion() {
        return this.questionRepository.findAll();
    }
//****** getQuestionById
    public Question getQuestionById(int id_q)
    {
        Question q=new Question();
        Optional<Question>Qo= this.questionRepository.findById(id_q);
        if(Qo.isPresent()){
            q=Qo.get();
        }
        return  q;
    }

    public  List<Choix> getChoixsByQuestionId(int id_q){
        Question q=new Question();
        List<Choix>listc=new ArrayList<Choix>();
        Optional<Question>Qo= this.questionRepository.findById(id_q);

        if(Qo.isPresent()){
            q=Qo.get();
            listc=q.getChoixs();
        }
        return  listc;
    }

    //** update QuestionQuiz

    public  Question SaveOrUpdateQuestion(Question question)
    {
    return  this.questionRepository.save(question);
    }

    //******save question

    public Question save(Question question)
    {
        return  this.questionRepository.save(question);


    }
    public Question saveOneQuestion(Question question){
        Question q=this.questionRepository.save(question);
        List<Choix>choixList=question.getChoixs();
        System.out.println(" question saved ******* "+q.toString());

        for(int i=0;i<choixList.size();i++){
            Choix choix=new Choix();
            Choix choixsaved=new Choix();

            choix=choixList.get(i);
            //save new choix to return id choix
            choix.setQuestion(q);
            choixsaved=this.choixRepository.save(choix);
            System.out.println(" choix saved ******* "+choixsaved.toString()+"\n");

        }
        return  q;
    }
    public List<Question>getAllQuestionUnique() {

        return  this.questionRepository.getAllQuestionDistinct();

    }



    public List<Question>getAllQuestionDistinct(){
        return  this.questionRepository.getAllQuestionDistinct();
    }
    public void delete(Question question){
        this.questionRepository.delete(question);
    }

    public QuestionRepository getQuestionRepository() {
        return questionRepository;
    }

    public void setQuestionRepository(QuestionRepository questionRepository) {
        this.questionRepository = questionRepository;
    }
}
