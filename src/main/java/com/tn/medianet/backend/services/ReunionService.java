package com.tn.medianet.backend.services;


import com.tn.medianet.backend.dao.ReunionRepository;
import com.tn.medianet.backend.model.Reunion;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ReunionService {

    @Autowired
    private ReunionRepository reunionRepository;

    public List<Reunion>getAllReunion(){

        return this.reunionRepository.findAll();

    }

    public Reunion saveReunion(Reunion reunion){
        return  this.reunionRepository.save(reunion);
    }


}
