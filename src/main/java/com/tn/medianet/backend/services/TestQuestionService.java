package com.tn.medianet.backend.services;


import com.tn.medianet.backend.dao.TestQuestionRepository;
import com.tn.medianet.backend.dao.TestRepository;
import com.tn.medianet.backend.model.Question;
import com.tn.medianet.backend.model.Test;
import com.tn.medianet.backend.model.Testquestion;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class TestQuestionService {

    @Autowired
    private TestQuestionRepository testQuestionRepository;

    @Autowired
    private TestRepository testRepository;


    public List<Testquestion>getAllTestQuestion()
    {
        return  this.testQuestionRepository.findAll();
    }


    //******************* getAllQuestionByIdTest
    public  List<Question>getAllQuestionByIdTest(int id) {
       // Optional<Test> ot = this.testRepository.findById(id);
        List<Question> listq = new ArrayList<Question>();
        List<Testquestion>listTQ=this.testQuestionRepository.findTestquestionByIdTest(id);

        for(int i=0;i<listTQ.size();i++)
        {
            Question q=new Question();
            q.setIdQuestion(listTQ.get(i).getQuestion().getIdQuestion());
            q.setDesgnQuestion(listTQ.get(i).getQuestion().getDesgnQuestion());
            q.setTempsQuestion(listTQ.get(i).getQuestion().getTempsQuestion());
            q.setTheme(listTQ.get(i).getQuestion().getTheme());
            q.setUnite(listTQ.get(i).getQuestion().getUnite());
            listq.add(q);
           // afficheText("listq "+listTQ.get(i).toString());
            afficheText("question "+i+" "+listq.get(i).toString());
        }
        afficheText("size liste "+listq.size());
        return listq;

    }

    public int getTestDuration(int id_test){

        return this.testQuestionRepository.getTestDuration(id_test);
    }


    public  Testquestion ajouter(Testquestion testquestion)
    {
        return this.testQuestionRepository.save(testquestion);
    }
    public void afficheText(String msg)
    {
        System.out.println(msg);
    }






    public TestQuestionRepository getTestQuestionRepository() {
        return testQuestionRepository;
    }

    public void setTestQuestionRepository(TestQuestionRepository testQuestionRepository) {
        this.testQuestionRepository = testQuestionRepository;
    }

    public TestRepository getTestRepository() {
        return testRepository;
    }

    public void setTestRepository(TestRepository testRepository) {
        this.testRepository = testRepository;
    }
}
